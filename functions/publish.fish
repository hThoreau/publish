set -l cmd (command basename (status -f) | command cut -f 1 -d '.')
function $cmd -V cmd -d "Publish the contents of the clipboard, a file or several, in its entirety or partially, in a pastebin"

  # Load dependencies
  source (dirname (status -f))/../instructions.fish
  source (dirname (status -f))/../dependency.fish
  function "$cmd"_main -V cmd
    dependency -n publish grep pastebinit \
    (type -qf termux-info; or echo xclip)
    or return 1
  
    # Parse arguments
    if argparse -n $cmd -x (string join -- ' -x ' L,v,h {L,v,h},{l,b,e,f,i,j,m,t,P,u,p} | string split ' ') 'l/lines=+' 'a/author=' 'b/pastebin=' 'e/echo' 'f/format=' 'h/help' 'i/filename' 'L/list' 'j/jabberid=' 'm/permatag=' 't/title=+' 'P/private' 'u/username=' 'p/password=' 'v/version' -- $argv 2>&1 | read err
      err $err
      reg "Use |$cmd -h| to see examples of valid syntaxes"
      return 1
    end

    # Test for options where no argument has to be passed
    if set -l flag (set --names | string match -r '(?<=^_flag_)[hvL]$')
      string match -q h $flag
      and "$cmd"_instructions
      or pastebinit -(string lower $flag) 2>/dev/null
      test -n "$argv"
      and return 1
      return 0
    end

    # Set clipboard
    set -l clipboard_in 'xclip -selection "clipboard"'
    set -l clipboard_out 'xclip -selection "clipboard" -o'
    if type -qf termux-info
      if not dpkg -s termux-api >/dev/null 2>&1
        err "$cmd: |termux-api| is required to manage clipboard content."
        reg "See installation instructions for it at https://wiki.termux.com/wiki/Termux:API"
        return 1
      end
      set clipboard_in 'termux-clipboard-set'
      set clipboard_out 'termux-clipboard-get'
    end

    # Declare variables
    set -l files $argv
    set -l lines $_flag_lines
    set --query _flag_author
    and set _flag_a -a $_flag_a
    set --query _flag_pastebin
    and set _flag_b -b $_flag_b
    set --query _flag_echo
    and set _flag_e -E
    set --query _flag_format
    and set _flag_f -f $_flag_f
    set --query _flag_jabberid
    and set _flag_j -j $_flag_j
    set --query _flag_permatag
    and set _flag_m -m $_flag_m
    set --query _flag_title
    and set -l titles -t\ {(string join , $_flag_title)}
    set --query _flag_username
    and set _flag_u -u $_flag_u
    set --query _flag_password
    and set _flag_p -p $_flag_p
    set -l flags $_flag_a $_flag_b $_flag_e $_flag_f $_flag_i $_flag_j $_flag_m $_flag_P $_flag_u $_flag_p

    # If no files were passed as an argument, check for content in the clipboard
    set -l tmp (command mktemp)
    if test -z "$files" -o (count $lines) -gt (count $files)
      if not string length -q -- (eval $clipboard_out)
        err "$cmd: No file specified nor clipboard content available to publish"
        return 1
      end
      read -n 1 -p 'wrn -n "Clipboard content available. Publish it? [y/n]: "' | string match -qir y
      or return 1
      eval $clipboard_out > $tmp
      set files $tmp
    end

    # Check line range validity
    if set -l invalid (string match -er '[^0-9,-]' $lines)
      test (count $invalid) -gt 1
      and set invalid "s |"(string join '|, |' $invalid)"|"
      or set invalid " |$invalid|"
      err "$cmd: Invalid line description$invalid"
      $cmd_instructions "$cmd -l/--lines"
      return 1
    else
      for i in (seq (count $lines))
        set lines[$i] (string replace -a '-' '..' $lines[$i] \
        | string replace ',' ' ')
      end
    end

    # Upload content
    set -l failed
    for i in (seq (count $files))

      # Load file
      if not test -e "$files[$i]"
        err "$cmd: file |$files[$i]| not found"
        set failed true; continue
      end
      set -l content (cat "$files[$i]")
      test -n "$lines[$i]"; and set content $content[$lines[$i]]

      # Check connection
      set -l url
      if not printf '%s\n' $content \
      | pastebinit $flags $titles[$i] 2>"$PREFIX"/tmp/err \
      | read url
        set -l message (sed -n \$p "$PREFIX"/tmp/err)
        string match -qr -- "/tmp/.+" "$files[$i]"
        and err "$cmd: Failed to send content"
        or err "$cmd: Failed to send file |"(basename $files[$i])"|"
        reg $message
        set failed true
        continue
      end

      # Check if content was refused
      if string match -qr '\.\w+/?$' $url
        string match -qr -- "/tmp/.+" "$files[$i]"
        and err "$cmd: Content flagged as spam."
        or err "$cmd: Contents of file |"(basename $files[$i])"| flagged as spam."
        test (wc -l $files[$i] | string match -r '^\d+') -lt 3
        and reg "Try to have at least tree lines of content for eash paste"
        set failed true
        continue
      end

      # Present content URL
      if string match -qr -- "/tmp/.+" "$files[$i]"
        test -n "$title[$i]"
        and reg "Content pasted at |$url| with title |$title[$i]|" 2>&1
        or reg "Content pasted at |$url|" 2>&1
      else
        test -n "$title[$i]"
        and reg "File |"(basename $files[$i])"| pasted at |$url| with title |$title[$i]|" 2>&1
        or reg "File |"(basename $files[$i])"| pasted at |$url|" 2>&1
      end
      test (count $files) -eq 1
      or continue
      echo $url | eval $clipboard_in
      win "URL copied to clipboard"
    end
    rm $tmp 2>/dev/null
    test -z "$failed"
  end

  # Add content passed through the stdin as a temp file argument
  set -l tmp (command mktemp)
  if not isatty stdin
    while read -l line
      echo $line >> $tmp
    end
    set -a argv $tmp
  end

  "$cmd"_main $argv
  set -l exit_status $status
  functions -e "$cmd"_{main,instructions} dependency
  test $exit_status -eq 0
end
